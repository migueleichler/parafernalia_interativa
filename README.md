# Desafio Parafernalia Interativa

- Comando para executar servidor de Desenvolvimento: python3 manage.py runserver --settings=app_voos.settings.develop
- Comando para importar dados da Planilha para Desenvolvimento: python3 manage.py import_br_flights --settings=app_voos.settings.develop
- Comando para executar testes: python3 manage.py test --settings=app_voos.settings.unittest


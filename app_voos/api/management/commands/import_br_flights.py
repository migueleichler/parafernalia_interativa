import csv
from datetime import datetime

from django.core.management.base import BaseCommand

from api.models import Flight


class Command(BaseCommand):
    def handle(self, *args, **options):
        with open('api/management/BrFlights2.csv', newline='', encoding='latin-1') as csvfile:
            flights_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            header = next(flights_reader, None)
            print(header)
            for row in flights_reader:
                print(row[1])
                print(row[3])
                print(row[10])
                print(row[14])
                flight = Flight(
                             companhia_aerea=row[1],
                             destination=datetime.strptime(row[3], '%Y-%m-%dT%H:%M:%SZ'),
                             location_from=row[10],
                             location_to=row[14],
                         )
                flight.save()


#'Companhia.Aerea'
#'Partida.Prevista'
#'Cidade.Origem'
#'Cidade.Destino'

from rest_framework import serializers

from api.models import Flight


class FlightSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flight
        fields = ('id', 'from', 'to', 'destination')

FlightSerializer._declared_fields['from'] = serializers.CharField(source='location_from')
FlightSerializer._declared_fields['to'] = serializers.CharField(source='location_to')

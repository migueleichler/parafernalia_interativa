from django.db import models


class Flight(models.Model):
    location_from = models.CharField(max_length=500, db_column='from')
    location_to = models.CharField(max_length=500, db_column='to')
    companhia_aerea = models.CharField(max_length=500)
    destination = models.DateTimeField(db_column='date')

from datetime import datetime

from rest_framework.generics import ListCreateAPIView

from api.models import Flight
from api.serializers import FlightSerializer


class FlightListCreateView(ListCreateAPIView):
    queryset = Flight.objects.all()
    serializer_class = FlightSerializer

    def get_queryset(self):
        queryset = Flight.objects.all()

        destination = self.request.query_params.get('destination')
        location_from = self.request.query_params.get('from')
        location_to = self.request.query_params.get('to')

        if destination:
            date = datetime.strptime(destination, '%Y-%m-%d').date()
            queryset = queryset.filter(destination__date=date)
        if location_from:
            queryset = queryset.filter(location_from=location_from)
        if location_to:
            queryset = queryset.filter(location_to=location_to)

        return queryset

from datetime import datetime

from django.test import TestCase, Client
from model_bakery import baker


class FlightListCreateViewTestCase(TestCase):

    def setUp(self):
        self.flight = baker.make('api.Flight')

        self.client = Client()
        self.url = '/flights'
        self.url_params = {
            'destination': self.flight.destination.date(),
            'from': self.flight.location_from,
            'to': self.flight.location_to,
        }
        self.post_data = {
            'destination': self.flight.destination,
            'from': self.flight.location_from,
            'to': self.flight.location_to,
        }

    def test_get_flights(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_get_flights_with_params(self):
        response = self.client.get(self.url, self.url_params)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()[0]['destination'][:10],
            datetime.strftime(self.url_params['destination'], '%Y-%m-%d')
        )
        self.assertEqual(response.json()[0]['from'], self.url_params['from'])
        self.assertEqual(response.json()[0]['to'], self.url_params['to'])

    def test_post_flights(self):
        response = self.client.post(self.url, self.post_data)
        self.assertEqual(response.status_code, 201)

from django.test import TestCase
from model_bakery import baker


class FlightTestCase(TestCase):

    def setUp(self):
        self.flight = baker.prepare('api.Flight')
        self.model_fields = list(self.flight.__dict__.keys())
        self.model_fields.remove('_state')

    def test_model_fields(self):
        self.assertTrue(
            self.model_fields,
            ['id', 'companhia_aerea', 'destination', 'location_from', 'location_to']
        )

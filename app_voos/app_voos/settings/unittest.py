from app_voos.settings.defaults import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'test.db',
    }
}

MIGRATION_MODULES = {
    'api': None,
}
